#!/bin/bash
backupdir=$1
if [[ ! -d $backupdir ]]
then
echo "Given directory does not exist!"
exit 1
fi
if [[ $# -ne 1 ]] 
then
echo "Wrong input!"
exit 1
else
echo "Checking Directory... OK!"
fi
if [[ ! $backupdir =~ '/'$ ]]
then
backupdir=$backupdir'/'
fi
backupfiles=$(ls $backupdir)
bcdir=/var/tmp/backup/
if [[ ! -d $bcdir ]]
then
mkdir $bcdir
fi
for backupfile in $backupfiles
do
filecheck=$(ls $bcdir$backupfile 2>> /dev/NULL)
if [[ $? -eq 0 ]]
then
echo "File $backupfile already exists!"
echo "Do you want to replace(r), rename(e), skip(s), ${backupfile}1(d)"
read ans
case $ans in
r|R)
cp -f $backupdir$backupfile $bcdir$backupfile
echo "Replacing file $backupdir$backupfile -->> $bcdir$backupfile"
;;
e|E)
echo "Enter new name"
read newfile
cp $backupdir$backupfile $bcdir$newfile
echo "Copying file $backupdir$backupfile -->> $bcdir$newfile"
;;
s|S)
continue
;;
d|D)
cp $backupdir$backupfile ${bcdir}${backupfile}1
echo "Copying file $backupdir$backupfile -->> ${bcdir}${backupfile}1"
;;
*)
echo "Wrong input. Skipping."
continue
;;
esac
else
cp $backupdir$backupfile $bcdir$backupfile
echo "Copying file $backupdir$backupfile -->> $bcdir$backupfile"
fi
done
