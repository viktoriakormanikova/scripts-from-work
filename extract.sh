#!/bin/bash

file=$1

if [ -z $file ]
then echo "Parameter was not given"

dirforarchive=${file%.*}

elif [[ -d $dirforarchive ]];

then echo "Destination folder exists" 
exit 1

elif [ -e $file ]
then
read -p "Do you really want to extract $file? Press [y] / [n] " ans

fi

if [ ans != "n" ]
then echo "Extracting to $file/"
fi

if [[ $file == *".tar.bz2" ]]
then tar xjf "$file"
elif [[ $file == *".tar.gz" ]]
then tar xzf "$file"
elif [[ $file == *".bz2" ]]
then bunzip2 xzf "$file"
elif [[ $file == *".rar" ]]
then rar x "$file"
elif [[ $file == *".gz" ]]
then gunzip "$file"
elif [[ $file == *".tar" ]]
then tar xf "$file"
elif [[ $file == *".tgz" ]]
then tar xzf "$file"
elif [[ $file == *".zip" ]]
then unzip "$file"
elif [[ $file == *".7z" ]]
then 7za x "$file"

else echo "Archive type unknown"

fi
