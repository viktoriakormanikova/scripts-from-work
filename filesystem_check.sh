#!/bin/bash
fss=$(df -h)
clearifs=$IFS
IFS=$'\n'
for fs in $fss
do
    fsname=$(echo "$fs" | awk '{print $1}')
    fsper=$(echo "$fs" | awk '{print $5}' | awk -F '%' '{print $1}')
    if [[ $fsper -ge 90 ]]
        then
        echo "Filesystem $fsname is almost full!"
    else
        echo "Filesystem $fsname is ok!"
    fi
done
IFS=$clearifs